
import ReactDOM from 'react-dom';
import React from 'react';
import './index.css';
import './photoAlbum.css';
import './sidebar.css';
import App from './App';



ReactDOM.render(<App />, document.getElementById('root'));


