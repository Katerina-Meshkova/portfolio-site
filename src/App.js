import React from 'react';
import Sidebar from "./Sidebar.js";
import PhotoAlbum from "./PhotoAlbum";

function App() {
    let obj = {
        albumTitle: 'Ребята',
        albumId: 1,
        srcOfPhotos : [
            {
                "id" : "1",
                "src" :'image_1.jpg'
            },
            {
                "id" : "2",
                "src" :'image_2_main.jpg'
            },
            {
                "id" : "3",
                "src" :'image_3.jpg'
            },
            {
                "id" : "4",
                "src" :'image_4.jpg'
            },
            {
                "id" : "5",
                "src" :'image_5.jpg'
            },
            {
                "id" : "6",
                "src" :'image_6.jpg'
            }
        ]
    };
    const topPaddingElement = obj.srcOfPhotos[obj.srcOfPhotos.length/3];
    topPaddingElement.isTop = 'top';

  return(
      <div className='wrapper'>
          <Sidebar />
          <PhotoAlbum albumTitle = {obj.albumTitle} albumId = {obj.albumId} srcOfPhotos={obj.srcOfPhotos} />
      </div>
  )
}

export default App;
