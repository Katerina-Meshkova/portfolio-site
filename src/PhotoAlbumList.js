import React from "react";


export default function PhotoAlbumList(props) {
    const {photos, albumId} = props;
    const firstArr = [];
    const secondArr = [];
    const thirdArr = [];
    /*    const photosElements = photos.map(photo =>
            <div key={photo.id} className={`item ${photo.isTop}`}><img src={`img/series${albumId}/${photo.src}`} alt="" className="item-img"/></div>
        );*/
    for (let i = 0; i < photos.length; i++) {
        if ((i >= 0) && (i < photos.length / 3)) {
            firstArr.push(photos[i]);

        }
        if ((i >= photos.length / 3) && i < (photos.length - photos.length / 3)) {
            secondArr.push(photos[i]);
        }
        if (i >= (photos.length - photos.length / 3)) {
            thirdArr.push(photos[i]);
        }

    }
    const firstElements = firstArr.map(photo =>
        <div key={photo.id} className={`item ${photo.isTop ? photo.isTop : ''}`}><img
            src={`img/album${albumId}/${photo.src}`} alt=""
            className="item-img"/></div>
    );
    const secondElements = secondArr.map(photo =>
        <div key={photo.id} className={`item ${photo.isTop ? photo.isTop : ''}`}><img
            src={`img/album${albumId}/${photo.src}`} alt=""
            className="item-img"/></div>
    );
    const thirdElements = thirdArr.map(photo =>
        <div key={photo.id} className={`item ${photo.isTop ? photo.isTop : ''}`}><img
            src={`img/album${albumId}/${photo.src}`} alt=""
            className="item-img"/></div>
    );
    return (
        /*      <div className="items">
            {photosElements}
        </div>*/
        <div className="container">
            <div className="items">{firstElements}</div>
            <div className="items">{secondElements}</div>
            <div className="items">{thirdElements}</div>
        </div>

    )
}

