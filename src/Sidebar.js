import React from "react";


 function Sidebar() {
    return (<div className='sidebar'>
        <div className="sidebar__header">Воронкова</div>
        <div className="sidebar__header">Юля</div>
        <div className='sidebar__list'>
            <div className='portfolio'>Портфолио</div>
            <div className='about'>Об авторе</div>
            <div className='conditions'>Условия</div>
        </div>
    </div>)
}
export default Sidebar;