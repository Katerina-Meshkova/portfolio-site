import React from "react";
import PhotoAlbumList from "./PhotoAlbumList";

export default function PhotoAlbum(props) {
    const{albumTitle, albumId, srcOfPhotos} = props;
    return (
        <div className='photo-album'>
            <div className="photo-album__header">
                <div className="arrow-left">Предыдущая серия</div>
                <div className="photo-album__header__title">{albumTitle}</div>
                <div className="arrow-right">Следующая серия</div>
            </div>
            <div className="photo-album__container">
            <PhotoAlbumList photos={srcOfPhotos} albumId= {albumId}/>
            </div>
        </div>
    )
}